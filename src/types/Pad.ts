export default interface IPadData {
    play?: boolean,
}

export default interface IGlobalPlayData {
    state: boolean,
}