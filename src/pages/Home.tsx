import React, {useState, useEffect, useReducer} from "react";

import Pad from "../components/Pad";
import styled from "styled-components";
import IGlobalPlayData from "../types/Pad";

const HomeWrap = styled.div`
  display: flex;
  height: 100vh;
  align-items: center;
  justify-content: center;
`;

const PadWrap = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(100px, 1fr));
  align-items: center;
  justify-content: center;
  justify-items: center;
`;


const Inner = styled.div`
  display: flex;
  flex-direction: column;
  flex-basis: 300px;
`;


const Controls = styled.div`
  display: flex;
  justify-content: center;
`;


const ControlButton = styled.button`
  border-radius: 50%;
  padding: 10px 12px;
  margin: 0 5px;
  border: 1px solid gray;
  color: gray;
  background-color: transparent;

  &:hover {
    color: black;
    border: 1px solid black;
  }

  &:active {
    color: #821059;
    border: 1px solid #821059;

  }
`;

const loops = [
    'https://s3.us-west-2.amazonaws.com/secure.notion-static.com/d7dd9b34-c459-4812-8d5a-79e98cfd1041/src_assets_loops_9-hk_mus125_lovefunk2_Gm.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Credential=AKIAT73L2G45EIPT3X45%2F20220312%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20220312T064548Z&X-Amz-Expires=86400&X-Amz-Signature=3cd0fc314d15c6f29ac1ecb8f8be6c443878a8194c1bcee22e818e874415be38&X-Amz-SignedHeaders=host&x-id=GetObject',
    'https://s3.us-west-2.amazonaws.com/secure.notion-static.com/e20b4c3a-c84a-48f6-b20e-b86f4f642968/src_assets_loops_8-hk_top125_zulu.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Credential=AKIAT73L2G45EIPT3X45%2F20220312%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20220312T064548Z&X-Amz-Expires=86400&X-Amz-Signature=d07b15510ae50ed82b859d96daeb96c44d1888ff64f3eade4c386a9a47780db3&X-Amz-SignedHeaders=host&x-id=GetObject',
    'https://s3.us-west-2.amazonaws.com/secure.notion-static.com/3800db1a-6979-4edb-80a2-06ace4c89d3d/src_assets_loops_6-hk_syn125_holdme1_Gm.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Credential=AKIAT73L2G45EIPT3X45%2F20220312%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20220312T064548Z&X-Amz-Expires=86400&X-Amz-Signature=261228f16ab3ead2124440081ede1555f12960ca31534075e8a5c2a8d96b886d&X-Amz-SignedHeaders=host&x-id=GetObject',
    'https://s3.us-west-2.amazonaws.com/secure.notion-static.com/6afa151d-883a-439f-8818-138edf40407c/src_assets_loops_7-hk_top125_latint.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Credential=AKIAT73L2G45EIPT3X45%2F20220312%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20220312T064548Z&X-Amz-Expires=86400&X-Amz-Signature=223f45ce58973eb1dd49a49cf114090bc300ef853eb2c9ef6d83574dbf4b0fb4&X-Amz-SignedHeaders=host&x-id=GetObject',
    'https://s3.us-west-2.amazonaws.com/secure.notion-static.com/3f92898a-5f1e-4e05-9522-f9c15dd0f8d0/src_assets_loops_5-hk_gtr125_pickcut_Gm.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Credential=AKIAT73L2G45EIPT3X45%2F20220312%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20220312T064548Z&X-Amz-Expires=86400&X-Amz-Signature=39d77357706bdcb034bddbd3631cd7f6c03998cbe3278b088bebaec390b9685e&X-Amz-SignedHeaders=host&x-id=GetObject',
    'https://s3.us-west-2.amazonaws.com/secure.notion-static.com/76e17663-e965-4cbe-affa-a0f34131c20b/src_assets_loops_3-GHS_123_Gm_Sun_Synth_Bass.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Credential=AKIAT73L2G45EIPT3X45%2F20220312%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20220312T064548Z&X-Amz-Expires=86400&X-Amz-Signature=441068d958712196e7e19dd4a8bf8ed6bf477baefd7fdc3df8c0ab6c42568889&X-Amz-SignedHeaders=host&x-id=GetObject',
    'https://s3.us-west-2.amazonaws.com/secure.notion-static.com/3880f3ab-b766-45f0-9a1b-c94d9bfbddff/src_assets_loops_1-GHS_123_Filo_Kick_Clap.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Credential=AKIAT73L2G45EIPT3X45%2F20220312%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20220312T064548Z&X-Amz-Expires=86400&X-Amz-Signature=0f73f6d1db732cac0e97f6b5020677e501eb50fbcfaaa259719ad29457f6a1a6&X-Amz-SignedHeaders=host&x-id=GetObject',
    'https://s3.us-west-2.amazonaws.com/secure.notion-static.com/dad4294e-94c4-47d9-bb4d-d397e6f0aff3/Untitled.mpga?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Credential=AKIAT73L2G45EIPT3X45%2F20220312%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20220312T064548Z&X-Amz-Expires=86400&X-Amz-Signature=e5cbf50957fec91b2e90ec40c662840dc528ed779d2f6cc5138bdba45d116159&X-Amz-SignedHeaders=host&x-id=GetObject',
    'https://s3.us-west-2.amazonaws.com/secure.notion-static.com/25ba418a-a38f-4a5f-bb3f-0d80ef84f575/Untitled.mpga?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Credential=AKIAT73L2G45EIPT3X45%2F20220312%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20220312T064548Z&X-Amz-Expires=86400&X-Amz-Signature=6efb17455a95ff5bb3777229256812a8a1efb0b09b43f3b866a3e3208f8da4e9&X-Amz-SignedHeaders=host&x-id=GetObject'
]
let intervalId: null | ReturnType<typeof setTimeout> = null

const Home: React.FC = () => {
    const [playInterval, setPlayInterval] = useReducer(x => x + 1, 0);

    const initialGlobalPlayState = {
        state: false,
    };
    const [playAll, setPlayAll] = useState<IGlobalPlayData>(initialGlobalPlayState);
    const [stopAll, setStopAllState] = useState<IGlobalPlayData>(initialGlobalPlayState);
    const [padOnCounter, setPadOnCounter] = useState<number>(0);

    useEffect(() => {

        if (playAll.state || stopAll.state) {
            if (intervalId) {
                clearInterval(intervalId);
                intervalId = null;
                if (stopAll.state) {
                    return
                }
            }
        }
        if (padOnCounter) {
            intervalId = setInterval(setPlayInterval, 8000);
        }

    }, [playAll, stopAll])// eslint-disable-line

    useEffect(() => {
        if (padOnCounter && !intervalId) {
            intervalId = setInterval(setPlayInterval, 8000);
        }
        if (!padOnCounter && intervalId) {
            clearInterval(intervalId);
            intervalId = null;
        }
    }, [padOnCounter])

    return (
        <HomeWrap>
            <Inner>
                <Controls>
                    <ControlButton
                        className="fa-solid fa-play"
                        onClick={() => {
                            setStopAllState({state: false})
                            setPlayAll({state: true})
                        }}
                    />
                    <ControlButton
                        className="fa-solid fa-pause"
                        onClick={
                            () => {
                                setPlayAll({state: false})
                                setStopAllState({state: true})
                            }
                        }
                    />

                </Controls>

                <PadWrap>
                    {
                        loops.map((loop, index) => {
                            return (
                                <Pad
                                    key={loop}
                                    title={'loop ' + (index + 1)}
                                    play={playAll}
                                    stop={stopAll}
                                    streamUrl={loop}
                                    playInterval={playInterval}
                                    setPadOnCounter={setPadOnCounter}
                                />
                            )
                        })
                    }
                </PadWrap>
            </Inner>
        </HomeWrap>
    );
};

export default Home;
