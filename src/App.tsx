import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import '@fortawesome/fontawesome-free/css/all.css';

import Home from "./pages/Home";

const App: React.FC = () => {
    return (
        <Home/>
    );
}

export default App;
