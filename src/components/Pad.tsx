import React, {useState, useEffect, useRef} from "react";
import styled from 'styled-components'
import PowerSwitch from "./PowerSwitch";
import IGlobalPlayData from "../types/Pad";

const PadWrap = styled.div`
  display: inline-flex;
  padding: 10px;
  flex-direction: column;
  box-shadow: 0 3px 4px 0 rgb(0 0 0 / 14%), 0 1px 8px 0 rgb(0 0 0 / 12%), 0 2px 3px -1px rgb(0 0 0 / 20%);
  margin: 10px;
  border-radius: 7px;

  &:hover {
    box-shadow: 0 2px 12px rgb(0 0 0 / 40%);
  }
`;

const Title = styled.div`

`;


const PowerSwitchWrap = styled(PowerSwitch)`
  margin-left: auto;
`;


interface Props {
    title: string;
    streamUrl: string;
    play: IGlobalPlayData;
    stop: IGlobalPlayData;
    playInterval: number;
    setPadOnCounter: any;
}


const Pad: React.FC<Props> = ({title, streamUrl, setPadOnCounter, playInterval, play, stop}: Props) => {
    const audioPlayerRef = useRef<HTMLAudioElement>(null);
    const [playState, setPlayState] = useState<boolean>(false);


    function changePlayState(play: boolean) {
        if (!play) {
            audioPlayerRef.current?.pause();
        }

        setPlayState(play)
    }

    function onPowerClick(currentPlayState: boolean) {
        if (currentPlayState) {
            setPadOnCounter((current: number) => current - 1)
            changePlayState(false)
        } else {
            setPadOnCounter((current: number) => current + 1)
            changePlayState(true)
        }
    }

    function playAudio() {
        if (audioPlayerRef.current) {
            audioPlayerRef.current.pause();
            audioPlayerRef.current.currentTime = 0;
            audioPlayerRef.current.play();
        }
    }

    useEffect(() => {
        if (play.state && playState) {
            playAudio();
        }
        if (stop.state) {
            audioPlayerRef.current?.pause();
        }

    }, [play, stop]);// eslint-disable-line

    useEffect(() => {
        if (playInterval && playState && !stop.state) {
            playAudio();
        }
    }, [playInterval]);// eslint-disable-line

    return (
        <PadWrap>
            <PowerSwitchWrap isOn={playState} onClick={() => onPowerClick(playState)}/>
            <Title>
                {title}
            </Title>

            <audio
                ref={audioPlayerRef}
                preload="auto"
                src={streamUrl}
            />
        </PadWrap>
    );
};

export default Pad;
