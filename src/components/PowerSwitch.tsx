import React from "react";
import styled, {css} from 'styled-components'

interface PowerSwitchProps {
    readonly isOn: boolean;
}

const PowerSwitchWrap = styled.div`
  cursor: pointer;
`;

const PowerSwitchIcon = styled.i<PowerSwitchProps>`
  display: flex;
  background: radial-gradient(circle closest-side, white, transparent);
  border-radius: 50%;

  ${({isOn}) =>
          isOn
                  ? css`
                    color: #1fcb37;

                  `
                  : css`
                    color: gray;

                  `
  }
  &:hover {
    box-shadow: 0 3px 4px 0 rgb(0 0 0 / 14%), 0 1px 8px 0 rgb(0 0 0 / 12%), 0 2px 3px -1px rgb(0 0 0 / 20%);
  }
`;

interface Props {
    isOn: boolean;
    onClick: any;
}

const PowerSwitch: React.FC<Props> = ({isOn, onClick, ...props}: Props) => {

    return (
        <PowerSwitchWrap {...props}>
            <PowerSwitchIcon isOn={isOn} onClick={onClick} className="fa-solid fa-power-off"/>
        </PowerSwitchWrap>
    );
};

export default PowerSwitch;
